#!/usr/bin/env mdl
SAMPLE_N = 8000*3
SENSOR_N = 50  # 50
SOURCE_N = 20  # 20
LEARN_N = 8000

REG_FIELD = "regular_field.dat"
RAND_FIELD = "random_field.dat"
DRIFT_SAMPLES = "drift_samples.dat"
SIGNAL_SAMPLES = "signal_samples.dat"
