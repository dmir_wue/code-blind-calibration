#!/usr/bin/env mdl
import os
import numpy as np
from scipy.special import expit

from statsmodels.tsa.arima_process import ArmaProcess
import matplotlib.pylab as pl
from scipy.signal import convolve, firwin
from numpy.linalg import norm, svd, inv, qr


H5FILE_PATH = os.path.join(
    os.path.abspath(os.path.dirname(__file__)),
    "senics_temperature"
)


def generate_signal(n):
    p = ArmaProcess([1.0, -0.85, -0.14], [1.0, -0.2])
    x = p.generate_sample(n)
    fir = firwin(256, 1e-5)
    y = convolve(x, fir)
    return (y[127:-128] + 17)/30


def gen_drift(n_drift, n_samples, t_start):
    d = np.zeros(n_samples)
    # return np.linspace(-5, 6, n_drift)
    e = min(t_start+n_drift, n_samples-1)
    d[t_start:e] = expit(np.linspace(-1, 6, n_drift))[:e-t_start]
    d[t_start:e] -= d[t_start]
    d[e:] = d[e-1]
    return d


def gen_rand_drift(n_samples, t_start, sigma=.001):
    d = np.zeros(n_samples)
    for i in range(t_start, n_samples):
        d[i] = d[i-1] + sigma * np.random.randn()

    fir = firwin(64, 1e-3)
    df = convolve(d, fir)
    return df[:d.shape[0]]


def awgn(x, snr=20.0):
    noise = np.zeros(x.shape)
    for i in range(x.shape[0]):
        avg_energy = np.std(x[i, :])
        snr_l = 10 ** (snr/20.0)
        noise_A = avg_energy / snr_l
        noise[i, :] = np.random.normal(0, noise_A, size=x[i, :].shape)
    return x + noise


def mse(d, de):
    e = np.linalg.norm(d - de)
    return e / np.linalg.norm(d)
