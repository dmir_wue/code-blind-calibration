import numpy as np
import matplotlib.pyplot as pl
import random
from numpy.random import rand, randint
from collections import namedtuple


Location = namedtuple(
    'Location',
    ('x', 'y'),
)

Source = namedtuple(
    'Source',
    ('id', 'loc', 'scale', 'offset'),
)

Sensor = namedtuple(
    'Sensor',
    ('id', 'loc'),
)


def random_field(n_sources=9, n_sensors=20, size=12, regularify=False):
    r = size / 2.0
    sources = []
    for sid in range(n_sources):
        while 1:
            x = rand()
            rand(); rand()
            y = rand()
            rand(); rand()
            loc = Location(size*x-r, size*y-r)
            # loc = Location(size*rand()-r, size*rand()-r)
            if regularify:
                min_d = 10000
                for s in sources:
                    d = (s.loc.x - loc.x)**2 + (s.loc.y - loc.y)**2
                    if d < min_d:
                        min_d = d
                if min_d < (size/10)**2:
                    print(min_d)
                    continue
            if loc.x**2 + loc.y**2 < (r*.9)**2:
                break

        sources.append(
            Source(id=sid, loc=loc, scale=10*rand()+10, offset=10*rand()+10)
        )

    sensors = []
    for sid in range(n_sensors):
        while 1:
            x = rand()
            rand(); rand()
            y = rand()
            rand(); rand()
            loc = Location(size*x-r, size*y-r)
            if loc.x**2 + loc.y**2 > r**2:
                continue
            if regularify:
                min_d = 10000
                for s in sources:
                    d = (s.loc.x - loc.x)**2 + (s.loc.y - loc.y)**2
                    if d < min_d:
                        min_d = d
                if min_d < 2 or min_d > 8:
                    continue
                min_d = 10000
                for s in sensors:
                    d = (s.loc.x - loc.x)**2 + (s.loc.y - loc.y)**2
                    if d < min_d:
                        min_d = d
                if min_d < 1:
                    continue
            break
        sensors.append(
            Sensor(id=sid, loc=loc)
        )

    return sources, sensors


def round_array(n_sources=9, n_sensors=20, size=12):
    r = size / 2.4
    sensors, sources = [], []
    if n_sources == 9 and n_sensors == 20:
        r1, r2 = 1*r/3, r*2/3

        for sid in range(9):
            phi = (sid)*(2*np.pi)/9
            loc = Location(r2*np.sin(phi), r2*np.cos(phi))
            sources.append(
                Source(id=sid, loc=loc, scale=18, offset=3)
            )

        for i, sid in enumerate(random.sample(range(5), 5)):
            phi = (i+1)*(2*np.pi)/5
            loc = Location(r1*np.sin(phi), r1*np.cos(phi))
            sensors.append(Sensor(id=sid, loc=loc))

        for i, sid in enumerate(random.sample(range(5, 20), 15)):
            phi = (i+1)*(2*np.pi)/15
            loc = Location(r*np.sin(phi), r*np.cos(phi))
            sensors.append(Sensor(id=sid, loc=loc))

        return sources, sensors

    else:
        raise Exception("unknown arrangement")


def round_signal_random_sensor_field(n_sources=9, n_sensors=20, size=12):
    r = size / 2.0
    sources, sensors = [], []
    if n_sources == 9 and n_sensors == 20:
        r1, r2 = 1.4*r/3, r*1.8/3
        sources.append(
            Source(id=0, loc=Location(.0, .0), scale=10*rand()+10, offset=5)
        )
        for sid in range(1, 9):
            phi = (sid-1)*(2*np.pi)/8
            loc = Location(r2*np.sin(phi), r2*np.cos(phi))
            sources.append(
                Source(id=sid, loc=loc, scale=18, offset=3)
            )

        for sid in range(n_sensors):
            while 1:
                loc = Location(size*rand()-r, size*rand()-r)
                if loc.x**2 + loc.y**2 < r**2:
                    break
            sensors.append(
                Sensor(id=sid, loc=loc)
            )

        return sources, sensors

    else:
        raise Exception("unknown arrangement")


def plot(sources, sensors, figsize=(7, 5), field_size=12, show=True):
    pl.figure(figsize=figsize)
    pl.scatter(
        [s.loc.x for s in sources], [s.loc.y for s in sources],
        color='r', marker='^', label="Sources", s=60)
    pl.scatter(
        [s.loc.x for s in sensors], [s.loc.y for s in sensors],
        color='b', marker='o', label="Sensors", s=30)
    for s in sensors:
        pl.text(s.loc.x+0.1, s.loc.y+0.1, "%d" % s.id)

    pl.xlim((-field_size//2-2, field_size//2+2))
    pl.ylim((-field_size//2-2, field_size//2+2))
    pl.tight_layout()
    if show:
        pl.show()
