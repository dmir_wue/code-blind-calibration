import argparse
import numpy as np
import pickle

from helpers import generate_signal
from common_const import SAMPLE_N, SOURCE_N

parser = argparse.ArgumentParser()
parser.add_argument("--non-stationary", action="store_true", help="generate non-stationary signal")
args = parser.parse_args()

SIGNAL_SAMPLES = "signal_samples.dat"

trends = [
    np.sin(np.linspace(-np.pi, np.pi/2, SAMPLE_N))+1,
    np.sin(np.linspace(0, np.pi*3/2, SAMPLE_N))+1,
]

signal_samples = np.zeros((SOURCE_N, SAMPLE_N))
for i in range(SOURCE_N):
    signal_samples[i, :] = generate_signal(SAMPLE_N)
    if args.non_stationary:
        T = .5 * (np.random.rand() + 0.4) * (np.abs(signal_samples[i, :]).max())
        trend = trends[np.random.randint(len(trends))] * T
        print(T, np.abs(trend).max())
        signal_samples[i, :] += trend

pickle.dump(signal_samples, open(SIGNAL_SAMPLES, 'wb'))
