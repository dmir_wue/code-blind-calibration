#!/usr/bin/env mdl
import os
import sys
import json
import pickle
import numpy as np
import pandas as pd
import random

from scipy.sparse.csgraph import minimum_spanning_tree

from . import fields

# def load_field_sensors(field_file):
#     sys.modules['fields'] = fields
#     with open(field_file, 'rb') as f:
#         ss = pickle.load(f)
#     sys.modules.pop('fields')

#     return ss['sensors']


# def load_field(field_file):
#     sys.modules['fields'] = fields
#     with open(field_file, 'rb') as f:
#         ss = pickle.load(f)
#     sys.modules.pop('fields')

#     return ss


# def get_reordering_matrix(field_file, nearest_neighbor=True, MST=False):
#     sys.modules['fields'] = fields
#     with open(field_file, 'rb') as f:
#         ss = pickle.load(f)
#     sys.modules.pop('fields')

#     sensors = ss['sensors']
#     SENSOR_N = len(sensors)
#     if MST:
#         # idx = []
#         G = np.zeros((SENSOR_N, SENSOR_N))

#         for i, s1 in enumerate(sensors):
#             for j, s2 in enumerate(sensors):
#                 if j <= i:
#                     continue
#                 d = np.sqrt((s1.loc.x - s2.loc.x)**2 + (s1.loc.y - s2.loc.y)**2)
#                 G[i, j] = d

#         mst = minimum_spanning_tree(G).tocoo()
#         E = np.zeros_like(G, dtype=np.bool)
#         for i, j in zip(mst.row, mst.col):
#             E[i, j] = True
#             E[j, i] = True

#         path, circuit = [0], []
#         nodes = np.array(range(SENSOR_N))
#         while len(path):
#             cur = path[-1]
#             avail = nodes[E[cur, :]]
#             if len(avail) == 0:
#                 circuit.append(path.pop(-1))
#                 continue
#             next_hop = avail[0]
#             E[cur, next_hop] = False
#             path.append(next_hop)

#         M1 = np.zeros((len(circuit), SENSOR_N))
#         for i, j in enumerate(circuit):
#             M1[i, j] = 1

#         M2 = M1.T.copy()

#         for i in range(M2.shape[0]):
#             M2[i, :] /= M2[i, :].sum()

#     elif nearest_neighbor:
#         M1 = np.zeros((SENSOR_N, SENSOR_N))
#         idx = [0]
#         visited = {0}
#         cur = 0
#         while len(visited) < len(sensors):
#             s = sensors[cur]
#             min_d = 2**32
#             nearest = None
#             for i, n in enumerate(sensors):
#                 if i in visited:
#                     continue
#                 d = (s.loc.x - n.loc.x)**2 + (s.loc.y - n.loc.y)**2
#                 if d < min_d:
#                     min_d = d
#                     nearest = i

#             visited.add(nearest)
#             idx.append(nearest)
#             cur = nearest

#         for i, j in enumerate(idx):
#             M1[i, j] = 1

#         M2 = M1.T.copy()
#     else:
#         M1 = np.eye(SENSOR_N)
#         M2 = M1.copy()

#     return M1, M2


def load_simulated_data_v2(field_file, signal_file, coeff=None, reorder=False, disorder=False, nonlinearity=None):
    sys.modules['fields'] = fields
    with open(field_file, 'rb') as f:
        ss = pickle.load(f)
    sys.modules.pop('fields')

    with open(signal_file, 'rb') as f:
        signal_samples = pickle.load(f)

    sensors, sources = ss['sensors'], ss['sources']
    if reorder:
        idx = [0]
        visited = {0}
        cur = 0
        if not disorder:
            while len(visited) < len(sensors):
                s = sensors[cur]
                min_d = 2**32
                nearest = None
                for i, n in enumerate(sensors):
                    if i in visited:
                        continue
                    d = (s.loc.x - n.loc.x)**2 + (s.loc.y - n.loc.y)**2
                    if d < min_d:
                        min_d = d
                        nearest = i

                visited.add(nearest)
                idx.append(nearest)
                cur = nearest
        else:
            while len(visited) < len(sensors):
                s = sensors[cur]
                max_d = 0
                farest = None
                for i, n in enumerate(sensors):
                    if i in visited:
                        continue
                    d = (s.loc.x - n.loc.x)**2 + (s.loc.y - n.loc.y)**2
                    if d > max_d:
                        max_d = d
                        farest = i

                visited.add(farest)
                idx.append(farest)
                cur = farest

        sensors = [fields.Sensor(id=i, loc=sensors[j].loc) for i, j in enumerate(idx)]

    # fields.plot(sources, sensors)
    SOURCE_N, SAMPLE_N = signal_samples.shape
    SENSOR_N = len(sensors)
    src_s = np.zeros((SOURCE_N, SAMPLE_N))

    if not coeff:
        def coeff(d):
            return 1.0 / (d**1.5+1)

    for i, src in enumerate(sources):
        src_s[i, :] = (signal_samples[i])*src.scale+src.offset

    G = np.zeros((SENSOR_N, SOURCE_N))
    for i, s in enumerate(sensors):
        for j, src in enumerate(sources):
            d = np.sqrt((src.loc.x - s.loc.x)**2 + (src.loc.y - s.loc.y)**2)
            G[i, j] = coeff(d)

    if nonlinearity is None:
        X = G.dot(src_s)
    elif nonlinearity == 'sqrt':
        X = np.sqrt((G**2).dot(src_s**2))
        # X = np.zeros((SENSOR_N, SAMPLE_N))
        for i in range(SENSOR_N):
            sa, sb = G[i, :].argsort()[-2:]
            X[i, :] += np.sqrt(
                G[i, sa] * src_s[sa, :] * G[i, sb] * src_s[sb, :]
            )

    return X, src_s, sources, sensors
