import pickle
from common_const import SENSOR_N, SOURCE_N, REG_FIELD, RAND_FIELD
import fields


def coeff(d):
    return 1.0 / (d+1.0)**1.5


# sources, sensors = fields.round_array(n_sources=SOURCE_N, n_sensors=SENSOR_N, size=12)
# fields.plot(sources, sensors, figsize=(6,6))
# pickle.dump({'sensors': sensors, 'sources': sources}, open('_'+REG_FIELD, 'wb'))

# sources, sensors = fields.random_field(n_sources=SOURCE_N, n_sensors=SENSOR_N, size=12)
# fields.plot(sources, sensors, figsize=(6,6))
# pickle.dump({'sensors': sensors, 'sources': sources}, open('_'+RAND_FIELD, 'wb'))

# sources, sensors = fields.random_field(n_sources=SOURCE_N, n_sensors=SENSOR_N, size=12)
# fields.plot(sources, sensors, figsize=(6,6))
# pickle.dump({'sensors': sensors, 'sources': sources}, open('_'+RAND_FIELD, 'wb'))

sources, sensors = fields.random_field(n_sources=SOURCE_N, n_sensors=SENSOR_N, size=20, regularify=True)
# fields.plot(sources, sensors, figsize=(6, 6), field_size=20)
pickle.dump({'sensors': sensors, 'sources': sources}, open('_'+RAND_FIELD, 'wb'))
