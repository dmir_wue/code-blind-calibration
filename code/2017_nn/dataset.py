from signal_generator import load_data
import numpy as np
from matplotlib import pyplot as plt


def load_simulated_data(field_path="data/raw_data/random_field_20_50.dat", signal_path="data/raw_data/signal_samples_24k.dat"):
  (X, s_src, sources, sensors) = load_data.load_simulated_data_v2(field_path, signal_path)
  return X

def blockify_dataset(X, blocksize=10):
  sensor_n, ts_n = X.shape
  Y = np.zeros((sensor_n, ts_n-blocksize, blocksize))
  for i in range(blocksize):
      Y[:, :, i] = X[:, i:ts_n-blocksize+i].copy()
  Y = Y.transpose(1, 0, 2)
  return Y

def augment_signal(signal, std_initial, std_bias, std_drift, std_noise, drift_rate):
  noise = np.random.normal(0, std_noise, size=signal.shape)
  bias = np.random.normal(0, std_bias)

  sensor_n, measures_n = signal.shape
  drift_matrix = np.zeros(signal.shape)
  for i in range(sensor_n):
    r = np.random.uniform()
    if r <= drift_rate:
      sensor_starting_drift = np.random.normal(0, std_initial)
      drift_matrix[i,0] = sensor_starting_drift + bias
      for k in range(1, measures_n):
        drift_delta = np.random.normal(0, std_drift)
        drift_matrix[i,k] = drift_matrix[i, k-1] + drift_delta

  return signal, drift_matrix, signal + drift_matrix

def generate_patch_from_blockified_data(X, patch_size, std_initial, std_bias, std_drift, std_noise, drift_rate=.5):
  block_n, sensor_n, _ = X.shape

  # augment a random signal
  idx = np.random.randint(0, block_n)
  signal = X[idx]
  signal, drift, drifted_signal = augment_signal(signal, std_initial, std_bias, std_drift, std_noise, drift_rate)

  return {
    'signal': signal,
    'drifted': drifted_signal,
    'drift': drift
  }


class CNNDataset:

  def __init__(self, training_data, test_data):

    self.patch_size = 20
    self.batch_size = 64
    # The epoch size was somewhat deduced from the parametrized training settings
    # in the training module, such as learning_rate and drift parameters
    self.epoch_size = int(80_000 / 64 / 100)

    self.training_data = training_data
    self.training_data_block = blockify_dataset(self.training_data, self.patch_size)
    self.test_data = test_data
    self.test_data_block = blockify_dataset(self.test_data, self.patch_size)

  """
  Generates a batch of `self.batch_size` patches
  """
  def batch_generator(self, data, std_initial, std_bias, std_drift, std_noise, drift_rate=.5):

    signal = []
    drift = []
    drifted = []

    for i in range(self.batch_size):
      patch = generate_patch_from_blockified_data(
        data,
        self.patch_size,
        std_initial,
        std_bias,
        std_drift,
        std_noise,
        drift_rate
      )

      signal.append(patch['signal'])
      drift.append(patch['drift'])
      drifted.append(patch['drifted'])
    
    return np.array(signal), np.array(drift), np.array(drifted)

  """
  Generates the appropriate number of batches to fullfill one complete epoch
  """
  def epoch_generator(self, std_initial, std_bias, std_drift, std_noise):
    epoch = []
    for i in range(self.epoch_size):
      epoch.append(self.batch_generator(self.training_data_block, std_initial, std_bias, std_drift, std_noise))
    return np.array(epoch)



if __name__ == "__main__":

  raw_data = load_simulated_data()
  dataset = CNNDataset(raw_data[:, :9000], raw_data[:, 9000:10000])
  signal, drift, drifted = dataset.batch_generator(dataset.training_data_block, 0.5, 0.2, 0.02, 0)

  patch_n = drifted.shape[0]
  for patch in range(patch_n):
    drifted_patch, signal_patch = drifted[patch], signal[patch]
    sensor_n = drifted_patch.shape[0]

    for s in range(sensor_n):
      plt.figure()
      # plt.subplot(sensor_n/4, 4, s)
      plt.plot(drifted_patch[s], 'r--')
      plt.plot(signal_patch[s], 'b-')
      plt.show()
