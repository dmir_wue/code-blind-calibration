import tensorflow as tf
from models import tf_model as model
import dataset
import numpy as np
import time
import datetime


def get_learning_rate(epoch):
  # stage-1
  if epoch < 20:
    lr = 1e-3
  elif epoch < 40:
    lr = 1e-4
  elif epoch < 50:
    lr = 1e-5
  # stage-2
  elif epoch < 60:
    lr = 2e-4
  elif epoch < 70:
    lr = 1e-4
  else:
    lr = 1e-5
  return lr


def get_train_feed_dict(lr, mb, learning_rate_pl, placeholders):
  feed_dict = {
    learning_rate_pl: lr,
  }
  for k, pl in placeholders.items():
    if k in mb:
      feed_dict[pl] = mb[k]

  return feed_dict


"""
Returns std_0, std_b, std_d, std_n
"""
def get_training_deviations(iteration):
  if iteration < 50_000:
    return 0.5, 0.2, 0.02, 0
  else:
    return 1.5, 0.5, 0.03, 0.5


def main():

  simulated_data = dataset.load_simulated_data()
  ds = dataset.CNNDataset(simulated_data[:, :9000], simulated_data[:, 9000:10000])

  with tf.Graph().as_default(), tf.Session().as_default() as sess:

    # 128 for simulated data, rest is 64
    net = model.get(projection_space_dim=128)
    loss = net['loss']

    learning_rate = tf.placeholder(tf.float32, shape=[], name='learning_rate')
    train_op = (tf.train.AdamOptimizer(learning_rate=learning_rate).minimize(loss))
    train_outspec = (train_op, net['monitor_vars'])

    tf.summary.scalar('learning_rate', learning_rate)
    for k, v in net['monitor_vars'].items():
      tf.summary.scalar(k, v)

    for var in tf.trainable_variables():
      tf.summary.histogram(var.op.name, var)

    summary = tf.summary.merge_all()

    init = tf.global_variables_initializer()
    saver = tf.train.Saver()

    summary_writer=tf.summary.FileWriter(
      "/tmp/tfsummary", sess.graph,
    )
    sess.run(init)

    placeholders = net['inputs']

    print("Starting training...")
    
    iterations = 0
    best_loss = 100
    patches_since_improvement = 0
    iteration_of_last_improvement = 0

    # while True:
    for epoch_n in range(100):

      std_initial, std_bias, std_drift, std_noise = get_training_deviations(iterations)
      epoch = ds.epoch_generator(std_initial, std_bias, std_drift, std_noise)
      lr = get_learning_rate(epoch_n)
      for batch_idx, batch in enumerate(epoch):

        # a batch is 20 measurements for 50 sensors, 64 times
        signal, drift, drifted = batch
        patches_n = signal.shape[0]

        for patch_idx in range(patches_n):
          iterations = iterations + 1
          patch = {
            'signal': signal[np.newaxis, patch_idx],
            'drift': drift[np.newaxis, patch_idx],
            'drifted': drifted[np.newaxis, patch_idx]
          }
          feed_dict = get_train_feed_dict(
            lr, patch, learning_rate, placeholders,
          )
          _, monitor_vars = sess.run(train_outspec, feed_dict=feed_dict)

          # Reporting
          if iterations % 50 == 0:
            outputs = [
              "clock:{}:{}/{}".format(
                epoch_n, batch_idx, iterations
              )
            ] + [
              "{}:{:.2g}".format(k, float(v))
              for k, v in monitor_vars.items()
            ] + [
              "learning rate:{}".format(lr)
            ] + [
              "stds:{}/{}/{}/{}".format(std_initial, std_bias, std_drift, std_noise)
            ]

            print(" ".join(outputs))

          new_loss = monitor_vars['loss']
          if new_loss < best_loss:
            print("Found better loss {:.2g} -> {:.2g} after {} iterations ({})".format(best_loss, new_loss, patches_since_improvement, iteration_of_last_improvement))
            best_loss = new_loss
            patches_since_improvement = 0
            iteration_of_last_improvement = iterations

          patches_since_improvement = patches_since_improvement + 1

          if patch_idx % 10 == 0:
            summary_bytes = sess.run(summary, feed_dict=feed_dict)
            summary_writer.add_summary(summary_bytes, iterations)
            summary_writer.flush()

    save_path = saver.save(sess, "trained_nets/model.ckpt")
    print("Saved model after {} iterations under {}".format(iterations, save_path))

if __name__ == "__main__":
    main()
