import tensorflow as tf
import pickle
import numpy as np
from models import tf_model as model
import dataset
from matplotlib import pyplot as plt


# def load_dataset(path="datasets/basic.dat"):
#   with open(path, "rb") as f:
#     return pickle.load(f)

def classify(test_data_batch, model_path, graph_file):
  with tf.Session() as sess:
    net = model.get()
    saver = tf.train.import_meta_graph(model_path + "/" + graph_file)
    saver.restore(sess, tf.train.latest_checkpoint(model_path))

    tf.global_variables_initializer().run()
    tf.tables_initializer().run()

    # Load placeholders from network spec
    signal_out = net['outputs'][0]
    drifted_pl = net['inputs']['drifted']

    calibration_predictions = []
    for _, patch in enumerate(test_data_batch):
      patch = patch[np.newaxis]
      feed_dict = {
        drifted_pl: patch
      }
      calibrated = sess.run(signal_out, feed_dict)
      calibration_predictions.append(calibrated[0])

    return np.array(calibration_predictions)

def rmse(prediction, truth):
  return np.sqrt(np.linalg.norm(prediction - truth)**2/prediction.size)

def drift_rate(drift_n, sensor_n):
  if drift_n == 0:
    return 0
  else:
    return drift_n/sensor_n

def classify(sess, net, test_data_batch):
  # Load placeholders from network spec
  signal_out = net['outputs'][0]
  drifted_pl = net['inputs']['drifted']
  calibration_predictions = []
  for _, patch in enumerate(test_data_batch):
    patch = patch[np.newaxis]
    feed_dict = {
      drifted_pl: patch
    }
    calibrated = sess.run(signal_out, feed_dict)
    calibration_predictions.append(calibrated[0])

  return np.array(calibration_predictions)


if __name__ == "__main__":
  raw_data = dataset.load_simulated_data()
  ds = dataset.CNNDataset(raw_data[:, :9000], raw_data[:, 9000:10000])

  # parameter spec for drift generation
  std_initial, std_bias, std_drift, std_noise = 1.5, 0.5, 0.03, 0.5 # 0.5, 0.2, 0.02, 0 # 1.5, 0.5, 0.03, 0.5


  with tf.Session() as sess:
    net = model.get()
    saver = tf.train.import_meta_graph("trained_nets/80k-iterations/model.ckpt.meta")
    saver.restore(sess, tf.train.latest_checkpoint("trained_nets/80k-iterations"))

    tf.global_variables_initializer().run()
    tf.tables_initializer().run()

    crmse_means, drmse_means = [], []
    # run evaluation for any possible number of drifting sensors
    for drifted_sensor_n in range(0, 51):
      truth, _, drifted = ds.batch_generator(ds.test_data_block, std_initial, std_bias, std_drift, std_noise, drift_rate(drifted_sensor_n, 50))
      predictions = classify(sess, net, drifted)

      drmses, crmses = [], []
      # run evaluation for every patch in the generated batch
      patches_n = predictions.shape[0]
      for idx in range(patches_n):
        crmse = rmse(predictions[idx], truth[idx])
        drmse = rmse(drifted[idx], truth[idx])
        crmses.append(crmse)
        drmses.append(drmse)

      crmse_mean = np.mean(crmses)
      drmse_mean = np.mean(drmses)
      print("# Drifted: {} --- Drifted RMSE Mean: {:.6f} - Calibrated RMSE Mean: {:.6f}".format(drifted_sensor_n, drmse_mean, crmse_mean))

      crmse_means.append(crmse_mean)
      drmse_means.append(drmse_mean)

  plt.figure()
  plt.plot(np.zeros(50), "g-")
  plt.plot(drmse_means, "r-")
  plt.plot(crmse_means, "b-")
  plt.legend(["Signal Patch", "Drifted Patch", "Calibrated Patch"])
  plt.title("RMSE between patch and ground truth")
  plt.xlabel("# of drifted sensors")
  plt.ylabel("RMSE")
  plt.show()
