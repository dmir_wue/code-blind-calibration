#!/usr/bin/env python
import os
import numpy as np
import tensorflow as tf
import tflearn
from IPython import embed

SENSOR_N = 50
PRNET_VARIABLES = 'prnet_variables'
# UPDATE_OPS_COLLECTION = 'prnet_update_ops'


def _get_variable(name, shape, initializer, weight_decay=0, dtype=tf.float32, trainable=True):
    if weight_decay > 0:
        regularizer = tf.contrib.layers.l2_regularizer(weight_decay)
    else:
        regularizer = None
    collections = [tf.GraphKeys.GLOBAL_VARIABLES, PRNET_VARIABLES]
    return tf.get_variable(name, shape=shape, initializer=initializer, dtype=dtype, regularizer=regularizer, collections=collections, trainable=trainable)


def BN(x, name=None):
    return tflearn.layers.normalization.batch_normalization(x, epsilon=1e-4, decay=0.99, name=name)


def conv2d(x, kernel_shape, output_nr_channel, padding=(0, 0), stride=(1, 1), bias=True):
    c_in = x.get_shape()[-1]
    W_shape = kernel_shape + (c_in, output_nr_channel, )
    b_shape = (1, 1, 1, output_nr_channel, )

    W = _get_variable("W", W_shape, initializer=tf.contrib.layers.variance_scaling_initializer(factor=2.0))

    hp, wp = padding
    hs, ws = stride
    x = tf.pad(x, [[0, 0], [hp, hp], [wp, wp], [0, 0], ])
    x = tf.nn.conv2d(
        x, W, [1, hs, ws, 1], padding='VALID', data_format="NHWC",
    )

    if bias:
        b = _get_variable("b", b_shape, initializer=tf.constant_initializer(0.0))
        return x+b

    return x


def resblock(inpvar, output_channels, stride=1):
    x = inpvar
    c_in = x.get_shape().as_list()[-1]
    bottleneck_channels = output_channels // 4

    def get_stem_partial(x):
        with tf.variable_scope('1x1_shrink'):
            x = conv2d(
                x, kernel_shape=(1, 1), output_nr_channel=bottleneck_channels,
            )

        with tf.variable_scope('3x3'):
            x = BN(x, name="bn_2")
            x = tf.nn.relu(x)
            x = conv2d(
                x, kernel_shape=(3, 3),
                output_nr_channel=bottleneck_channels,
                padding=(1, 1),
            )
        with tf.variable_scope('1x1_expand'):
            x = BN(x, name="bn_3")
            x = tf.nn.relu(x)
            x = conv2d(
                x, kernel_shape=(1, 1),
                output_nr_channel=output_channels,
            )

        return x

    # common bn and relu
    channel_match = (output_channels == c_in)
    if stride == 1 and channel_match:
        shortcut = x
        x = BN(x, name="bn_1")
        x = tf.nn.relu(x)
        stem = get_stem_partial(x)
    else:
        if stride != (1, 1):
            _stride = (1, ) + stride + (1, )
            x = tf.nn.avg_pool(
                x, ksize=_stride, strides=_stride,
                padding='VALID', data_format='NHWC',
            )

        x = BN(x, name="bn_1")
        x = tf.nn.relu(x)
        stem = get_stem_partial(x)

        with tf.variable_scope('1x1_shortcut'):
            shortcut = conv2d(
                x, kernel_shape=(1, 1),
                output_nr_channel=output_channels,
            )

    assert(shortcut.get_shape().as_list()[1:] == stem.get_shape().as_list()[1:])
    return shortcut + stem


def group(name, x, output_channels, nr_block, stride):
    with tf.variable_scope(name):
        with tf.variable_scope("block-1"):
            x = resblock(x, output_channels, stride=stride)
        for i in range(2, nr_block + 1):
            with tf.variable_scope("block-{}".format(i)):
                x = resblock(x, output_channels)
    return x


def get(projection_space_dim=64):
    input_shape = (None, SENSOR_N, None)
    drifted = tf.placeholder(tf.float32, shape=input_shape, name="drifted")
    signal = tf.placeholder(tf.float32, shape=input_shape, name="signal")
    drift = tf.placeholder(tf.float32, shape=input_shape, name="drift")

    x_in = (tf.expand_dims(drifted, axis=-1) - 25) / 20
    drift_in = tf.expand_dims(drift, axis=-1) / 5

    def project(x):
        return conv2d(
            x, kernel_shape=(SENSOR_N, 7), output_nr_channel=projection_space_dim,
            padding=(0, 3), bias=False,
        )

    with tf.variable_scope('conv_proj') as scope:
        x_p = project(x_in)
        scope.reuse_variables()
        drift_p = project(drift_in)

    with tf.variable_scope('conv_reshape'):
        x = conv2d(x_p, kernel_shape=(1, 1), output_nr_channel=SENSOR_N*8)
        # (N, H, W, C)
        x = tf.squeeze(x, axis=1)
        # (N, W, C)
        xshape = tf.shape(x)
        x = tf.reshape(x, [xshape[0], xshape[1], SENSOR_N, 8])
        x = tf.transpose(x, [0, 2, 1, 3])

    assert(x.get_shape().as_list()[1:] == [SENSOR_N, None, 8])

    with tf.variable_scope('conv0'):
        x = conv2d(x, kernel_shape=(3, 3), padding=(1, 1), output_nr_channel=16)

    with tf.variable_scope('resnet'):
        x = group('g0', x, 64, 3, (1, 1))

    with tf.variable_scope("conv_fuse"):
        x = BN(x, name="bn_fuse")
        x = tf.nn.relu(x)
        x = conv2d(
            x, kernel_shape=(3, 3), padding=(1, 1),
            output_nr_channel=1,
        )

    x_drift = tf.squeeze(x, axis=-1)
    x_signal = drifted - x_drift

    loss_proj = tf.reduce_mean((x_p-drift_p)**2) / tf.reduce_mean((drift_p)**2)
    loss_recovery = tf.reduce_mean((x_signal-signal)**2)
    loss = loss_proj + loss_recovery
    loss_rmse = tf.sqrt(tf.losses.mean_squared_error(signal, x_signal))

    return {
        'inputs': {
            'drifted': drifted,
            'signal': signal,
            'drift': drift,
        },
        'outputs': (x_signal, ),
        'loss': loss,
        'monitor_vars': {
            'loss': loss,
            'loss_proj': loss_proj,
            'loss_recovery': loss_recovery,
            'loss_rmse': loss_rmse
        }
    }


if __name__ == "__main__":
    get()
